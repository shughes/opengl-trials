#include <termios.h>
#include <stdio.h>
#include <stdlib.h>
#include <cairo/cairo.h>
#include <linux/input.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <stdbool.h>

#define EGL_EGLEXT_PROTOTYPES
#define GL_GLEXT_PROTOTYPES

#include <gbm.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glext.h>
#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <linux/vt.h>
#include <linux/kd.h>
#include <signal.h>
#include <xf86drm.h>

#include "kms.h"

#ifdef GL_OES_EGL_image
static PFNGLEGLIMAGETARGETRENDERBUFFERSTORAGEOESPROC glEGLImageTargetRenderbufferStorageOES_func;
#endif

struct list {
   struct list *prev;
   struct list *next;
};

struct frame {
   uint32_t gl_renderbuffer;
   uint32_t fb_id;
   struct gbm_bo *bo;
   EGLImageKHR image;
};

struct tty {
   struct termios tp;
   int fd;
};

struct compositor {
   EGLDisplay egl_display;
   EGLContext egl_context;
   EGLConfig egl_config;
   struct {
      int id;
      int fd;
   } drm;
   struct gbm_device *gbm;
   struct gbm_bo *cursor_bo[2];
   uint32_t gl_framebuffer;
   struct frame frame[2];
   struct tty tty;
};

struct window {
   struct object *base;
   int x;
   int y; 
   int width;
   int height;
   int texture;
   int texture_width;
   int texture_height;
   cairo_surface_t *surface;
};


struct output {
   bool page_flip_pending;
};


#define container_of(ptr, sample, member) \
   (void *)((char *)(ptr) - ((char *)&(sample)->member))

static void list_init(struct list *list) {
   list->prev = list;
   list->next = list;
}

static void list_insert(struct list *list, struct list *node) {
   node->prev = list;
   node->next = list->next;
   list->next = node;
   node->next->prev = node;
}

static const char input_ev[] = "/dev/input/event3";
static const char device_name[] = "/dev/dri/card0";

#define M_PI 3.14159265358979323846 


