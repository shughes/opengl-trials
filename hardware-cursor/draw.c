#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

int main()
{
        int fbfd = 0;
        struct fb_var_screeninfo vinfo;
        struct fb_fix_screeninfo finfo;
        long int screensize = 0;
        char *fbp = 0;
        int x = 0, y = 0;
        long int location = 0;

        /* Open the file for reading and writing */
        fbfd = open("/dev/fb0", O_RDWR);
        if (!fbfd) {
                printf("Error: cannot open framebuffer device.\n");
                exit(1);
        }

	system("./sm 800 600 75 24");

        /* Get fixed screen information */
        if (ioctl(fbfd, FBIOGET_FSCREENINFO, &finfo)) {
                printf("Error reading fixed information.\n");
                exit(2);
        }

        /* Get variable screen information */
        if (ioctl(fbfd, FBIOGET_VSCREENINFO, &vinfo)) {
                printf("Error reading variable information.\n");
                exit(3);
        }
        
        

        /* Figure out the size of the screen in bytes */
        screensize = vinfo.xres * vinfo.yres * vinfo.bits_per_pixel / 8;
        
        /* Map the device to memory */
        fbp = (char *)mmap(0, screensize, PROT_READ | PROT_WRITE, MAP_SHARED,
                fbfd, 0);       
        if ((int)fbp == -1) { 
        	printf("Error: failed to map framebuffer device to memory.\n");
        	exit(5);
        }
        printf("The framebuffer device was mapped to memory successfully.\n");
	printf("Draw color pixels random. (range: x= 400-500, y= 400-500)\n");
	
	system("clear");
	usleep(1000);
	printf("*********************************************************\n");
	printf("******     Change mode to 800x600 75Hz 24 bpp     *******\n");
	printf("*********************************************************\n");

for(x=0 ;x<0x100; x++){
	for(y=0; y<0x100; y++){
	/* Where we are going to put the pixel */

        /* Figure out where in memory to put the pixel */
	location = (x+200) * (vinfo.bits_per_pixel/8) + (y+200)* finfo.line_length;

        *(fbp + location)     = x;   		/* blue  */
        *(fbp + location + 1) = 0xaa;		/* green */
        *(fbp + location + 2) = 0xff -y;	/* red   */
	}
}


        munmap(fbp, screensize);
        close(fbfd);
        return 0;
}
        
