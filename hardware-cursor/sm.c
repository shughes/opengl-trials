#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

void usage()
{
	printf("\nTVia.Inc FrameBuffer device set mode");
	printf("\n-------------------------------------");
	printf("\nusage:   sm [xres] [yres] [freq] [bpp]\n");
	printf("\nExample: sm 800 600 75 8\n");
	printf("\n");
	printf(" Resolution |     Freq     (hz)          |   Bpp     \n");
	printf("------------+----------------------------+-----------\n");
	printf("  640x440   |           60(NTSC)         | 8, 16, 24 \n");
	printf("  640x480   |   50(PAL),60(NTSC),72,75   | 8, 16, 24 \n");
	printf("  720x480   |   50(PAL),60(NTSC)         | 8, 16, 24 \n");
	printf("  720x540   |   50(PAL)                  | 8, 16, 24 \n");
	printf("  720x576   |   50(PAL)                  | 8, 16, 24 \n");
	printf("  768x576   |   50(PAL)                  | 8, 16, 24 \n");
	printf("  800x600   |   50(PAL),      60,72,75   | 8, 16, 24 \n");
	printf("  1024x768  |                 60,72,75   | 8, 16, /  \n");
	printf("  1280x1024 |                 60,72,75   | 8, / , /  \n");
	printf("  1600x1200 |                 60,/ , /   | 8, / , /  \n");
		
}

static int check[10][9]={
	{640, 440, 0,  60, 0,  0 , 8 , 16, 24},
	{640, 480, 50, 60, 72, 75, 8 , 16, 24},
	{720, 480, 50, 60, 0,  0 , 8 , 16, 24},
	{720, 540, 50, 0,  0,  0 , 8 , 16, 24},
	{800, 600, 50, 60, 72, 75, 8 , 16, 24},
	{768, 576, 50, 0,  0,  0 , 8 , 16, 24},
	{720, 576, 50, 0,  0,  0 , 8 , 16, 24},
	{1024,768, 0,  60, 72, 75, 8 , 16, 0 },
	{1280,1024,0,  60, 72, 75, 8 , 0,  0 },
	{1600,1200,0,  60, 0 , 0 , 8 , 0,  0 },
};

int modecheck(int c, char **v)
{
	int i,v1,v2,v3,v4;

	v1 = atoi( v[1] );
	v2 = atoi( v[2] );
	v3 = atoi( v[3] );
	v4 = atoi( v[4] );

	for(i=0;i<10;i++){
		if( v1 == check[i][0] && v2 == check[i][1] && v3 !=0 && 
					( v3 == check[i][2] 
					|| v3 == check[i][3] 
					|| v3 == check[i][4] 
					||v3 == check[i][5] )
					&& v4 !=0 && 
					( v4 == check[i][6] 
					|| v4 == check[i][7] 
					|| v4 == check[i][8] ) 
					) return 0;		
	}
	return -1;
}

int main(int argc, char **argv)
{
        int fbfd = 0;
        struct fb_var_screeninfo vinfo;
	
	if(argc != 5){
		usage();
		return 0;
	}
	
	if( modecheck(argc, argv) != 0 ){
		printf("\n*****Unavailable mode !******\n");
		usage();
		return 0;
	}
	
        /* Open the file for reading and writing */
        fbfd = open("/dev/fb0", O_RDWR);
        if (!fbfd) {
                printf("Error: cannot open framebuffer device.\n");
                exit(1);
        }

        /* Get variable screen information */
        if (ioctl(fbfd, FBIOGET_VSCREENINFO, &vinfo)) {
                printf("Error reading variable information.\n");
                exit(2);
        }
	
	vinfo.xres = vinfo.xres_virtual = atoi(argv[1]);
	vinfo.yres = vinfo.yres_virtual = atoi(argv[2]);
	vinfo.pixclock = atoi(argv[3]);
	vinfo.bits_per_pixel = atoi(argv[4]);

        
        /* Put variable screen information */
        if (ioctl(fbfd, FBIOPUT_VSCREENINFO, &vinfo)) {
                printf("Error writing variable information.\n");
                exit(3);
        }
        
        
        close(fbfd);
        return 0;
}
        
