#include "window-server.h"

static cairo_t* create_cairo_context(int width, int height, int channels, 
      cairo_surface_t** surf, unsigned char** buffer) {
   cairo_t* cr;

   *buffer = (unsigned char *) calloc (channels * width * height, sizeof (unsigned char));
   if (!*buffer) {
      printf ("create_cairo_context() - Couldn't allocate buffer\n");
      return NULL;
   }

   *surf = cairo_image_surface_create_for_data(*buffer, CAIRO_FORMAT_ARGB32,
         width, height, channels * width);

   if (cairo_surface_status(*surf) != CAIRO_STATUS_SUCCESS) {
      free (*buffer);
      printf ("create_cairo_context() - Couldn't create surface\n");
      return NULL;
   }

   cr = cairo_create(*surf);
   if(cairo_status(cr) != CAIRO_STATUS_SUCCESS) {
      free (*buffer);
      printf ("create_cairo_context() - Couldn't create context\n");
      return NULL;
   }

   return cr;
}

static cairo_surface_t *init_image(const char *file) {
   return cairo_image_surface_create_from_png(file);
}

static void render_background(cairo_t *context, const struct window *win) {
   cairo_surface_t *image = win->surface;
   int x, y;
   int w = win->width / win->texture_width + 1;
   int h = win->height / win->texture_height + 1;
   for(x = 0; x < w; x++) {
      for(y = 0; y < h; y++) {
         cairo_set_source_surface(context, image, win->texture_width*x, win->texture_height*y);
         cairo_paint(context);
      }
   }
}

static void render_window(cairo_t *context, const struct window *win) {
   cairo_set_source_surface(context, win->surface, win->x, win->y);
   cairo_paint(context);
}


void resize(int width, int height) {
   glViewport (0, 0, width, height);
   glMatrixMode (GL_PROJECTION);
   glLoadIdentity ();
   glOrtho (0.0f, (GLfloat) width, (GLfloat) 0.0f, (GLfloat) height, -1.0f, 1.0f);
   glClear(GL_COLOR_BUFFER_BIT);
}

void init_gl (int width, int height) {
   printf ("OpenGL version: %s\n", glGetString (GL_VERSION));
   printf ("OpenGL vendor: %s\n", glGetString (GL_VENDOR));
   printf ("OpenGL renderer: %s\n", glGetString (GL_RENDERER));

   glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
   glDisable(GL_DEPTH_TEST);
   glDisable(GL_BLEND);
   glEnable(GL_TEXTURE_RECTANGLE_ARB);

   resize(width, height);
}

static unsigned int init_texture(int width, int height) {
   GLuint texture_id;
   glDeleteTextures(1, &texture_id);
   glGenTextures(1, &texture_id);
   glBindTexture(GL_TEXTURE_RECTANGLE_ARB, texture_id);
   glTexImage2D(GL_TEXTURE_RECTANGLE_ARB,
         0,
         GL_RGBA,
         width,
         height,
         0,
         GL_BGRA,
         GL_UNSIGNED_BYTE,
         NULL);
   glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
   return texture_id;
}

static void init_view() {
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
   glClear(GL_COLOR_BUFFER_BIT);
}

static void set_texture_data(unsigned char *data, unsigned int texture_id, int width, int height) {
   glBindTexture(GL_TEXTURE_RECTANGLE_ARB, texture_id);
   glTexImage2D(GL_TEXTURE_RECTANGLE_ARB,
         0,
         GL_RGBA,
         width,
         height,
         0,
         GL_RGBA,
         GL_UNSIGNED_BYTE,
         data);

}

static void render_screen(const int texture, unsigned char *data, const int w, const int h) {
   int i, j;

   glTexImage2D(GL_TEXTURE_RECTANGLE_ARB,
         0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   glBindTexture(GL_TEXTURE_RECTANGLE_ARB, texture);
   glPushMatrix();
   {
      glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
      glBegin(GL_QUADS);
      {
         glTexCoord2f(0.0, 0);
         glVertex2f(0.0, 0.0);
         glTexCoord2f((GLfloat) w, 0.0);
         glVertex2f((GLfloat) w, 0.0);
         glTexCoord2f((GLfloat) w, (GLfloat) h);
         glVertex2f((GLfloat) w, (GLfloat) h);
         glTexCoord2f(0.0, (GLfloat) h);
         glVertex2f(0.0, (GLfloat) h);
      }
      glEnd();
   }
   glPopMatrix();
   glFinish();

}


static int init_egl(struct compositor *ec) {
   int n, ret, major, minor;
   static const EGLint attribs[] = {
      EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
      EGL_RED_SIZE, 1,
      EGL_GREEN_SIZE, 1,
      EGL_BLUE_SIZE, 1,
      EGL_ALPHA_SIZE, 0,
      EGL_DEPTH_SIZE, 1,
      EGL_RENDERABLE_TYPE, EGL_OPENGL_BIT,
      EGL_NONE
   };

   ec->gbm = gbm_create_device(ec->drm.fd);
   if (ec->gbm == NULL) {
      fprintf(stderr, "couldn't create gbm device\n");
      ret = -1;
      goto close_fd;
   }

   ec->egl_display = eglGetDisplay(ec->gbm);
   if (ec->egl_display == EGL_NO_DISPLAY) {
      fprintf(stderr, "eglGetDisplay() failed\n");
      ret = -1;
      goto destroy_gbm_device;
   }

   if (!eglInitialize(ec->egl_display, &major, &minor)) {
      printf("eglInitialize() failed\n");
      ret = -1;
      goto egl_terminate;
   }

   eglBindAPI(EGL_OPENGL_API);

   if (!eglChooseConfig(ec->egl_display, attribs, &ec->egl_config, 1, &n) || n != 1) {
      fprintf(stderr, "failed to choose argb config\n");
      goto egl_terminate;
   }

   ec->egl_context = eglCreateContext(ec->egl_display, 
         ec->egl_config, EGL_NO_CONTEXT, NULL);
   if (ec->egl_context == NULL) {
      fprintf(stderr, "failed to create context\n");
      ret = -1;
      goto egl_terminate;
   }

   if (!eglMakeCurrent(ec->egl_display, 
            EGL_NO_SURFACE, EGL_NO_SURFACE, ec->egl_context)) {
      fprintf(stderr, "failed to make context current\n");
      ret = -1;
      goto destroy_context;
   }

   return ret;

destroy_context:
   eglDestroyContext(ec->egl_display, ec->egl_context);
egl_terminate:
   eglTerminate(ec->egl_display);
destroy_gbm_device:
   gbm_device_destroy(ec->gbm);
close_fd:
   close(ec->drm.fd);
}

static void handle_page_flip(int fd, unsigned int frame,
      unsigned int sec, unsigned int usec, void *data) {
   struct output *output = (struct output *) data;
   output->page_flip_pending = false;
}

static void setup_frame(int i, int fd, struct compositor *ec, int width, int height) {
   int stride, handle, ret;

   glGenRenderbuffers(1, &ec->frame[i].gl_renderbuffer);
   glBindRenderbuffer(GL_RENDERBUFFER, ec->frame[i].gl_renderbuffer);

   ec->frame[i].bo = gbm_bo_create(ec->gbm, width, height,
         GBM_BO_FORMAT_XRGB8888, 
         GBM_BO_USE_SCANOUT | GBM_BO_USE_RENDERING);
   if(ec->frame[i].bo == NULL) {
      // gbm_bo_create error
   }

   ec->frame[i].image = eglCreateImageKHR(ec->egl_display, NULL, EGL_NATIVE_PIXMAP_KHR, 
         ec->frame[i].bo, NULL);
   if(ec->frame[i].image == EGL_NO_IMAGE_KHR) {
      // eglCreateImageKHR error
   }

   glEGLImageTargetRenderbufferStorageOES_func(GL_RENDERBUFFER, ec->frame[i].image);

   handle = gbm_bo_get_handle(ec->frame[i].bo).u32;
   stride = gbm_bo_get_stride(ec->frame[i].bo); 

   ret = drmModeAddFB(fd, width, height, 24, 32, stride, handle,
         &ec->frame[i].fb_id);
   if(ret) {
      // drmModeAddFB error
   }

   //ec->cursor_bo[i] = gbm_bo_create(ec->gbm, 64, 64, GBM_FORMAT_ARGB8888,
         //GBM_BO_USE_CURSOR_64X64 | GBM_BO_USE_WRITE);

}

static void setup_tty(struct tty *tty) {
   tty->fd = open("/dev/tty9", O_RDWR | O_NOCTTY | O_CLOEXEC); 
   ioctl(tty->fd, VT_ACTIVATE, 9);
   tcgetattr(tty->fd, &tty->tp);
   tty->tp.c_lflag &= ~ECHO;
   tcsetattr(tty->fd, TCSAFLUSH, &tty->tp);
}

int main(int argc, char *argv[])
{
   struct compositor *ec;
   uint32_t handle, stride;
   struct kms kms;
   int ret, fd;
   drmModeCrtcPtr saved_crtc;
   struct window *desktop = (struct window *) malloc(sizeof(*desktop));
   struct window *cursor = (struct window *) malloc(sizeof(*cursor));
   int k_fd, m_fd;
   struct input_event ev;
   int i, current, efd;
   struct epoll_event event;
   struct epoll_event *events;
   struct vt_mode mode = { 0 };
   drmEventContext event_ctx;
   struct output *output = (struct output *) malloc(sizeof(*output));

   cairo_surface_t *surface = NULL;
   unsigned char *surf_data;
   cairo_t *cr = NULL;
   uint32_t buf[64 * 64];

   ec = (struct compositor *) malloc(sizeof(struct compositor));
   setup_tty(&ec->tty);
   ec->drm.fd = open(device_name, O_RDWR);
   fd = ec->drm.fd;
   if (fd < 0) {
      fprintf(stderr, "couldn't open %s, skipping\n", device_name);
      return -1;
   }

   if(init_egl(ec) < 0) { 
      ret = -1;
      goto close_fd;
   }

   if (!setup_kms(fd, &kms)) {
      ret = -1;
      goto egl_terminate;
   }

   // Begin: OpenGL

#ifdef GL_OES_EGL_image
   glEGLImageTargetRenderbufferStorageOES_func =
      (PFNGLEGLIMAGETARGETRENDERBUFFERSTORAGEOESPROC)
      eglGetProcAddress("glEGLImageTargetRenderbufferStorageOES");
#else
   fprintf(stderr, "GL_OES_EGL_image not supported at compile time\n");
#endif

   glGenFramebuffers(1, &ec->gl_framebuffer);
   glBindFramebuffer(GL_FRAMEBUFFER, ec->gl_framebuffer);

   for(i = 0; i < 2; i++) {
      setup_frame(i, fd, ec, kms.mode.hdisplay, kms.mode.vdisplay); 
   }

   current = 0;
   saved_crtc = drmModeGetCrtc(fd, kms.encoder->crtc_id);
   ret = drmModeSetCrtc(fd, kms.encoder->crtc_id, ec->frame[current].fb_id, 0, 0,
         &kms.connector->connector_id, 1, &kms.mode);

   init_gl(kms.mode.hdisplay, kms.mode.vdisplay);

   desktop->width = kms.mode.hdisplay;
   desktop->height = kms.mode.vdisplay;
   desktop->texture_width = 59;
   desktop->texture_height = 34;
   desktop->x = 0;
   desktop->y = 0;
   desktop->surface = init_image("pattern.png");
   desktop->texture = init_texture(desktop->width, desktop->height); 

   glBindTexture(GL_TEXTURE_RECTANGLE_ARB, desktop->texture);

   cursor->width = 17;
   cursor->height = 30;
   cursor->x = 250;
   cursor->y = 250;
   cursor->surface = init_image("cursor.png");

   init_view();

   efd = epoll_create1(0);
   event.data.fd = fd;
   event.events = EPOLLIN;
   epoll_ctl(efd, EPOLL_CTL_ADD, fd, &event);
   events = (struct epoll_event *) calloc(1, sizeof event);
   output->page_flip_pending = false;

   k_fd = open(input_ev, O_RDONLY);

   while(true) {
      size_t rb = read(k_fd, &ev, sizeof(ev));
      bool changed = false;

      if((ev.value == 1) || (ev.value == 2)) {
         if(ev.code == 16) {
            break;
         }
         else if(ev.code == 108) {
            cursor->y += 20;
            changed = true;
         }
         else if(ev.code == 103) {
            cursor->y -= 20;
            changed = true;
         }
         else if(ev.code == 105) {
            cursor->x -= 20;
            changed = true;
         }
         else if(ev.code == 106) {
            cursor->x += 20;
            changed = true;
         }
      }

      if(changed == true) {
         if(output->page_flip_pending == false) {
            output->page_flip_pending = true;

            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, 
                  GL_RENDERBUFFER, ec->frame[current].gl_renderbuffer);

            cr = create_cairo_context(desktop->width, desktop->height, 4, &surface, &surf_data);
            cairo_new_path(cr); 
            render_background(cr, desktop);
            render_window(cr, cursor);
            render_screen(desktop->texture, surf_data, desktop->width, desktop->height);
            cairo_destroy(cr);

            /*
            memset(buf, 1, sizeof buf);
            gbm_bo_write(ec->cursor_bo[current], buf, sizeof buf);
            handle = gbm_bo_get_handle(ec->cursor_bo[current]).s32;
            drmModeSetCursor(ec->drm.fd, kms.encoder->crtc_id, handle, 64, 64);
            drmModeMoveCursor(ec->drm.fd, kms.encoder->crtc_id, 100, 100);
            */

            if(drmModePageFlip(fd, kms.encoder->crtc_id, ec->frame[current].fb_id, 
                     DRM_MODE_PAGE_FLIP_EVENT, output) < 0) {
               fprintf(stderr, "queueing pageflip failed\n");
            }

            epoll_wait(efd, events, 1, -1);

            memset(&event_ctx, 0, sizeof(event_ctx));
            event_ctx.version = DRM_EVENT_CONTEXT_VERSION;
            event_ctx.page_flip_handler = handle_page_flip;
            drmHandleEvent(fd, &event_ctx);
            current ^= 1;
         }

      }
   }


   ret = drmModeSetCrtc(fd, saved_crtc->crtc_id, saved_crtc->buffer_id,
         saved_crtc->x, saved_crtc->y,
         &kms.connector->connector_id, 1, &saved_crtc->mode);

   // End: OpenGL

destroy_cairo:
   cairo_surface_destroy(desktop->surface); 
   cairo_surface_destroy(cursor->surface); 
free_saved_crtc:
   drmModeFreeCrtc(saved_crtc);
rm_fb:
   drmModeRmFB(fd, ec->frame[0].fb_id);
   drmModeRmFB(fd, ec->frame[1].fb_id);
   eglMakeCurrent(ec->egl_display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
destroy_context:
   eglDestroyContext(ec->egl_display, ec->egl_context);
egl_terminate:
   eglTerminate(ec->egl_display);
destroy_gbm_device:
   gbm_device_destroy(ec->gbm);
close_fd:
   close(fd);
   close(k_fd);
   close(ec->tty.fd);
   free(ec);

   return ret;
}


/*
   m_fd = open("/dev/input/event7", O_RDONLY);
   k_fd = open(input_ev, O_RDONLY);
   */
/*
 *while(1) {
 int changed = 0, count;

 size_t rb = read(k_fd, &ev, sizeof(ev));

 if((ev.value == 1) || (ev.value == 2)) {
 if(ev.code == 16) {
 break;
 }
 else if(ev.code == 108) {
 win->y += 10;
 changed = 1;
 }
 else if(ev.code == 103) {
 win->y -= 10;
 changed = 1;
 }
 else if(ev.code == 105) {
 win->x -= 10;
 changed = 1;
 }
 else if(ev.code == 106) {
 win->x += 10;
 changed = 1;
 }
 }

 size_t mb = read(m_fd, &ev, sizeof(ev));
 if(ev.type == 3) {
 if(ev.code == 0) { 
 cursor->last_x = cursor->abs_x;
 cursor->abs_x = ev.value;
 } else if(ev.code == 1) { 
 cursor->last_y = cursor->abs_y;
 cursor->abs_y = ev.value; 
 } else if(ev.code == 24) {
 cursor->pressure = ev.value;
 } 

 if(cursor->pressure > 40) {
 int x_dist = cursor->abs_x - cursor->last_x;
 int y_dist = cursor->abs_y - cursor->last_y;
 int min_dist = 5;
 int change = 5;
 int vchange = 3;
 int v_dist = 10;

 x_dist = x_dist < 0 ? x_dist * -1 : x_dist;
 y_dist = y_dist < 0 ? y_dist * -1 : y_dist;

 if(cursor->down == 0) {
 cursor->last_x = cursor->abs_x;
 cursor->last_y = cursor->abs_y;
 }

 if(x_dist < min_dist) {

 } else if(cursor->abs_x > cursor->last_x) {
 win->x += change;
 } else if(cursor->abs_x < cursor->last_x) {
 win->x -= change;
 } 

 if(y_dist < v_dist) {

 } else if(cursor->abs_y > cursor->last_y) {
 win->y += vchange;
 } else if(cursor->abs_y < cursor->last_y) {
 win->y -= vchange;
 }

changed = 1;
cursor->down = 1;
} else {
   cursor->down = 0;
   cursor->last_x = cursor->abs_x;
   cursor->last_y = cursor->abs_y;
}

if(win->x > desktop->width) {
   win->x = desktop->width - 25;
} else if(win->x < 0) {
   win->x = 25;
} else if(win->y > desktop->height) {
   win->y = desktop->height - 25;
} else if(win->y < 0) {
   win->y = 25;
}
}

*/
