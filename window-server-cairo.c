#define _GNU_SOURCE
#include <stdio.h>
#include <stdbool.h>
#include <sys/mman.h>
#include <errno.h>
#include <stdlib.h>
#include <cairo/cairo.h>
#include <cairo/cairo-gl.h>
#include <linux/input.h>
#include <fcntl.h>
#include <sys/epoll.h>

#define EGL_EGLEXT_PROTOTYPES
#define GL_GLEXT_PROTOTYPES

#include <gbm.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glext.h>
#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <drm.h>
#include <xf86drmMode.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <linux/vt.h>
#include <linux/kd.h>
#include <signal.h>
#include <xf86drm.h>

#ifdef GL_OES_EGL_image
static PFNGLEGLIMAGETARGETRENDERBUFFERSTORAGEOESPROC glEGLImageTargetRenderbufferStorageOES_func;
#endif

struct list {
   struct list *prev;
   struct list *next;
};

struct frame {
   uint32_t gl_renderbuffer;
   uint32_t fb_id;
   struct gbm_bo *bo;
   EGLImageKHR image;
};

struct cursor {
   int pressure;
   int abs_x;
   int abs_y;
   int last_x;
   int last_y;
   int down;
   int x_diff;
   int y_diff;
   int x_begin;
   int y_begin;
};

struct modeset_buf {
	uint32_t width;
	uint32_t height;
	uint32_t stride;
	uint32_t size;
	uint32_t handle;
	uint8_t *map;
	uint32_t fb;
};

struct compositor {
   EGLSurface egl_surface;
   EGLDisplay egl_display;
   EGLContext egl_context;
   EGLConfig egl_config;
   struct gbm_device *gbm;
   struct gbm_surface *gbm_surface;
   cairo_surface_t *cairo_surface;
   cairo_device_t *cairo_device;

   uint8_t r, g, b;
	bool r_up, g_up, b_up;
   struct {
      int id;
      int fd;
   } drm;
   struct modeset_buf bufs[2];
};


struct kms {
   drmModeConnector *connector;
   drmModeEncoder *encoder;
   drmModeModeInfo mode;
};

static cairo_t* create_cairo_context(int width, int height, int channels, 
      cairo_surface_t** surf, unsigned char** buffer) {
   cairo_t* cr;

   *buffer = calloc (channels * width * height, sizeof (unsigned char));
   if (!*buffer) {
      printf ("create_cairo_context() - Couldn't allocate buffer\n");
      return NULL;
   }

   *surf = cairo_image_surface_create_for_data(*buffer, CAIRO_FORMAT_ARGB32,
         width, height, channels * width);

   if (cairo_surface_status(*surf) != CAIRO_STATUS_SUCCESS) {
      free (*buffer);
      printf ("create_cairo_context() - Couldn't create surface\n");
      return NULL;
   }

   cr = cairo_create(*surf);
   if (cairo_status(cr) != CAIRO_STATUS_SUCCESS) {
      free (*buffer);
      printf ("create_cairo_context() - Couldn't create context\n");
      return NULL;
   }

   return cr;
}


static unsigned char* get_image(char *file, int width, int height) {
   cairo_surface_t *surface = NULL;
   unsigned char *surf_data;
   cairo_t *cr = create_cairo_context(width, height, 4, &surface, &surf_data);
   int w, h;
   float w2, h2;
   cairo_surface_t *image;

   cairo_new_path (cr); 

   image = cairo_image_surface_create_from_png(file);
   w = cairo_image_surface_get_width(image);
   h = cairo_image_surface_get_height(image);

   w2 = (float) width / w;
   h2 = (float) height / h;
   
   cairo_scale(cr, w2, h2);

   cairo_set_source_surface(cr, image, 0, 0);
   cairo_paint(cr);
   cairo_surface_destroy(image); 
   cairo_destroy(cr);

   return surf_data;
}

static int init_egl(struct compositor *ec, int width, int height) {
   int n, ret, major, minor;
   static const EGLint attribs[] = {
      EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
      EGL_RED_SIZE, 1,
      EGL_GREEN_SIZE, 1,
      EGL_BLUE_SIZE, 1,
      EGL_ALPHA_SIZE, 0,
      EGL_DEPTH_SIZE, 1,
      EGL_RENDERABLE_TYPE, EGL_OPENGL_BIT,
      EGL_NONE
   };
   EGLSurface surface;

   ec->gbm = gbm_create_device(ec->drm.fd);
   if (ec->gbm == NULL) {
      fprintf(stderr, "couldn't create gbm device\n");
      ret = -1;
      goto close_fd;
   }

   ec->egl_display = eglGetDisplay(ec->gbm);
   if (ec->egl_display == EGL_NO_DISPLAY) {
      fprintf(stderr, "eglGetDisplay() failed\n");
      ret = -1;
      goto destroy_gbm_device;
   }

   if (!eglInitialize(ec->egl_display, &major, &minor)) {
      printf("eglInitialize() failed\n");
      ret = -1;
      goto egl_terminate;
   }

   eglBindAPI(EGL_OPENGL_API);

   if (!eglChooseConfig(ec->egl_display, attribs, &ec->egl_config, 1, &n) || n != 1) {
      fprintf(stderr, "failed to choose argb config\n");
      goto egl_terminate;
   }

   ec->egl_context = eglCreateContext(ec->egl_display, 
         ec->egl_config, EGL_NO_CONTEXT, NULL);
   if (ec->egl_context == NULL) {
      fprintf(stderr, "failed to create context\n");
      ret = -1;
      goto egl_terminate;
   }

   ec->gbm_surface = gbm_surface_create(ec->gbm, width, height,
			   GBM_BO_FORMAT_XRGB8888,
			   GBM_BO_USE_SCANOUT | GBM_BO_USE_RENDERING);

   ec->egl_surface = eglCreateWindowSurface(ec->egl_display, ec->egl_config, ec->gbm_surface, NULL);

   ec->cairo_device = cairo_egl_device_create(ec->egl_display, ec->egl_context); 

   ec->cairo_surface = cairo_gl_surface_create_for_egl(ec->cairo_device,
                     ec->egl_surface, width, height);

   //cairo_surface_set_user_data(ec->cairo_surface, 

   if (!eglMakeCurrent(ec->egl_display, 
            ec->egl_surface, ec->egl_surface, ec->egl_context)) {
      fprintf(stderr, "failed to make context current\n");
      ret = -1;
      goto destroy_context;
   }

   return ret;

destroy_context:
   eglDestroyContext(ec->egl_display, ec->egl_context);
egl_terminate:
   eglTerminate(ec->egl_display);
destroy_gbm_device:
   gbm_device_destroy(ec->gbm);
close_fd:
   close(ec->drm.fd);
}

static int modeset_create_fb(int fd, struct modeset_buf *buf)
{
	struct drm_mode_create_dumb creq;
	struct drm_mode_destroy_dumb dreq;
	struct drm_mode_map_dumb mreq;
	int ret;

	/* create dumb buffer */
	memset(&creq, 0, sizeof(creq));
	creq.width = buf->width;
	creq.height = buf->height;
	creq.bpp = 32;
	ret = drmIoctl(fd, DRM_IOCTL_MODE_CREATE_DUMB, &creq);
	if (ret < 0) {
		fprintf(stderr, "cannot create dumb buffer (%d): %m\n",
			errno);
		return -errno;
	}
	buf->stride = creq.pitch;
	buf->size = creq.size;
	buf->handle = creq.handle;

	/* create framebuffer object for the dumb-buffer */
	ret = drmModeAddFB(fd, buf->width, buf->height, 24, 32, buf->stride,
			   buf->handle, &buf->fb);
	if (ret) {
		fprintf(stderr, "cannot create framebuffer (%d): %m\n",
			errno);
		ret = -errno;
		goto err_destroy;
	}

	/* prepare buffer for memory mapping */
	memset(&mreq, 0, sizeof(mreq));
	mreq.handle = buf->handle;
	ret = drmIoctl(fd, DRM_IOCTL_MODE_MAP_DUMB, &mreq);
	if (ret) {
		fprintf(stderr, "cannot map dumb buffer (%d): %m\n",
			errno);
		ret = -errno;
		goto err_fb;
	}

	/* perform actual memory mapping */
	buf->map = mmap(0, buf->size, PROT_READ | PROT_WRITE, MAP_SHARED,
		        fd, mreq.offset);
	if (buf->map == MAP_FAILED) {
		fprintf(stderr, "cannot mmap dumb buffer (%d): %m\n",
			errno);
		ret = -errno;
		goto err_fb;
	}

	/* clear the framebuffer to 0 */
	memset(buf->map, 0, buf->size);

	return 0;

err_fb:
	drmModeRmFB(fd, buf->fb);
err_destroy:
	memset(&dreq, 0, sizeof(dreq));
	dreq.handle = buf->handle;
	drmIoctl(fd, DRM_IOCTL_MODE_DESTROY_DUMB, &dreq);
	return ret;
}

static EGLBoolean setup_kms(int fd, struct kms *kms)
{
   drmModeRes *resources;
   drmModeConnector *connector;
   drmModeEncoder *encoder;
   int i;

   resources = drmModeGetResources(fd);
   if (!resources) {
      fprintf(stderr, "drmModeGetResources failed\n");
      return EGL_FALSE;
   }

   for (i = 0; i < resources->count_connectors; i++) {
      connector = drmModeGetConnector(fd, resources->connectors[i]);
      if (connector == NULL)
         continue;

      if (connector->connection == DRM_MODE_CONNECTED &&
            connector->count_modes > 0)
         break;

      drmModeFreeConnector(connector);
   }

   if (i == resources->count_connectors) {
      fprintf(stderr, "No currently active connector found.\n");
      return EGL_FALSE;
   }

   for (i = 0; i < resources->count_encoders; i++) {
      encoder = drmModeGetEncoder(fd, resources->encoders[i]);

      if (encoder == NULL)
         continue;

      if (encoder->encoder_id == connector->encoder_id)
         break;

      drmModeFreeEncoder(encoder);
   }

   kms->connector = connector;
   kms->encoder = encoder;
   kms->mode = connector->modes[0];

   return EGL_TRUE;
}

static const char input_ev[] = "/dev/input/event3";
static const char device_name[] = "/dev/dri/card0";

#define container_of(ptr, sample, member) \
   (void *)((char *)(ptr) - ((char *)&(sample)->member))

static void list_init(struct list *list) {
   list->prev = list;
   list->next = list;
}

static void list_insert(struct list *list, struct list *node) {
   node->prev = list;
   node->next = list->next;
   list->next = node;
   node->next->prev = node;
}


#define M_PI 3.14159265358979323846 



struct output {
   bool page_flip_pending;
};


static void handle_page_flip(int fd, unsigned int frame,
      unsigned int sec, unsigned int usec, void *data) {

   struct output *out = (struct output *) data;
   if(out != NULL) {
      out->page_flip_pending = false;
   }
}

static uint8_t next_color(bool *up, uint8_t cur, unsigned int mod)
{
	uint8_t next;

	next = cur + (*up ? 1 : -1) * (rand() % mod);
	if ((*up && next < cur) || (!*up && next > cur)) {
		*up = !*up;
		next = cur;
	}

	return next;
}

int main(int argc, char *argv[])
{
   struct compositor *ec;
   struct kms kms;
   int ret, fd;
   drmModeCrtcPtr saved_crtc;
   int k_fd, m_fd;
   int i, current, efd, tty_fd;
   struct epoll_event event;
   struct epoll_event *events;
   struct vt_mode mode = { 0 };
   drmEventContext event_ctx;
   struct modeset_buf *buf;
   struct output *output;
   unsigned char* bg = NULL;
   cairo_t *cr;

   tty_fd = open("/dev/tty8", O_RDWR | O_NOCTTY | O_CLOEXEC); 
   ioctl(tty_fd, VT_ACTIVATE, 8);

   ec = malloc(sizeof(struct compositor));
   ec->drm.fd = open(device_name, O_RDWR);
   fd = ec->drm.fd;
   if (fd < 0) {
      fprintf(stderr, "couldn't open %s, skipping\n", device_name);
      return -1;
   }

   if (!setup_kms(fd, &kms)) {
      ret = -1;
   }

   //init_egl(ec, kms.mode.hdisplay, kms.mode.vdisplay);
   //cr = cairo_create(ec->cairo_surface);

   ec->bufs[0].width = kms.mode.hdisplay;
   ec->bufs[0].height = kms.mode.vdisplay;
   ec->bufs[1].width = kms.mode.hdisplay;
   ec->bufs[1].height = kms.mode.vdisplay;

   ec->r = rand() % 0xff;
   ec->g = rand() % 0xff;
   ec->b = rand() % 0xff;
   ec->r_up = ec->g_up = ec->b_up = true;

   modeset_create_fb(fd, &ec->bufs[0]);
   modeset_create_fb(fd, &ec->bufs[1]);

   output = malloc(sizeof(*output));
   output->page_flip_pending = false;

   efd = epoll_create1(0);
   event.data.fd = fd;
   event.events = EPOLLIN;
   epoll_ctl(efd, EPOLL_CTL_ADD, fd, &event);
   events = calloc(1, sizeof event);

   saved_crtc = drmModeGetCrtc(fd, kms.encoder->crtc_id);
   ret = drmModeSetCrtc(fd, kms.encoder->crtc_id, ec->bufs[0].fb, 0, 0,
         &kms.connector->connector_id, 1, &kms.mode);
   ret = drmModeSetCrtc(fd, kms.encoder->crtc_id, ec->bufs[1].fb, 0, 0,
         &kms.connector->connector_id, 1, &kms.mode);


   //bg = get_image("cursor.png", 17, 25);

   current = 0;
   for(i=0; i<1; i++) {
      int j;
      if(output->page_flip_pending == false) {

         output->page_flip_pending = true;

         ec->r = next_color(&ec->r_up, ec->r, 20);
         ec->g = next_color(&ec->g_up, ec->g, 10);
         ec->b = next_color(&ec->b_up, ec->b, 5);

         buf = &ec->bufs[current ^ 1];

         for (j = 0; j < buf->height; ++j) {
            int k, off;
            for (k = 0; k < buf->width; ++k) {
               off = buf->stride * j + k * 4;
               *(uint32_t*)&buf->map[off] = (ec->r << 16) | (ec->g << 8) | ec->b;
            }
         }

         /* for(j=0; j<425; j++) {
            *(uint8_t*)&buf->map[j] = bg[j];
         } */

         if(drmModePageFlip(fd, kms.encoder->crtc_id, buf->fb,
                  DRM_MODE_PAGE_FLIP_EVENT, output) < 0) {
            fprintf(stderr, "queueing pageflip failed\n");
         } 

         epoll_wait(efd, events, 1, -1);

         current ^= 1;
         memset(&event_ctx, 0, sizeof(event_ctx));
         event_ctx.version = DRM_EVENT_CONTEXT_VERSION;
         event_ctx.page_flip_handler = handle_page_flip;
         drmHandleEvent(fd, &event_ctx);

      }

      usleep(500000);
   }

   ret = drmModeSetCrtc(fd, saved_crtc->crtc_id, saved_crtc->buffer_id,
         saved_crtc->x, saved_crtc->y,
         &kms.connector->connector_id, 1, &saved_crtc->mode);


free_saved_crtc:
   drmModeFreeCrtc(saved_crtc);
rm_fb:
   drmModeRmFB(fd, ec->bufs[0].fb);
   drmModeRmFB(fd, ec->bufs[1].fb);
close_fd:
   close(fd);
   close(k_fd);
   close(m_fd);
   close(tty_fd);
   free(events);

   return ret;
}
